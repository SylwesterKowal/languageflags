<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://kowalcommerce.com/Kowal-Commerce-License.txt
 *
 * @category   BSS
 * @package    Kowal_LanguageFlags
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://kowalcommerce.com )
 * @license    http://kowalcommerce.com/Kowal-Commerce-License.txt
 */
 
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kowal_LanguageFlags',
    __DIR__
);
