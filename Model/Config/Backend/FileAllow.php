<?php

namespace Kowal\LanguageFlags\Model\Config\Backend;

use Magento\Config\Model\Config\Backend\File;

/**
 * Class FileAllow
 *
 * @package Kowal\LanguageFlags\Model\Config\Backend
 */
class FileAllow extends File
{
    /**
     * GetAllowedExtensions
     *
     * @return string[]
     */
    public function getAllowedExtensions()
    {
        return ['jpg', 'jpeg', 'gif', 'png'];
    }
}
