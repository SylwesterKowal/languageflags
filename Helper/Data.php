<?php

namespace Kowal\LanguageFlags\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 *
 * @package Kowal\LanguageFlags\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Data constructor.
     * @param StoreManagerInterface $storeManager
     * @param Context $context
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        Context $context
    ) {
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * GetEndableModule
     *
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getEndableModule()
    {
        return $this->scopeConfig->getValue(
            'kowal_language_flags/general/enable_module',
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * GetUpload
     *
     * @param number $storeId
     * @return string
     * @throws NoSuchEntityException
     */
    public function getUrlImageFlag($storeId = null)
    {
        $imgUrl = $this->scopeConfig
            ->getValue('kowal_language_flags/image/uploadflag', ScopeInterface::SCOPE_STORE, $storeId);
        if ($imgUrl != '') {
            return $this->storeManager->getStore($storeId)
                   ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'kowalstoresflags/' . $imgUrl;
        }
        return false;
    }

    /**
     * GetStoreId
     *
     * @return int
     * @throws NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * GetHeight
     *
     * @param number $storeId
     * @return mixed
     */
    public function getHeight($storeId = null)
    {
        return $this->scopeConfig->getValue(
            'kowal_language_flags/image/imgheight',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * GetWidth
     *
     * @param number $storeId
     * @return mixed
     */
    public function getWidth($storeId = null)
    {
        return $this->scopeConfig->getValue(
            'kowal_language_flags/image/imgwidth',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get Storeview Name
     *
     * @param number $storeId
     * @return mixed
     */
    public function getShowStoreviewName($storeId = null)
    {
        return $this->scopeConfig->getValue(
            'kowal_language_flags/store_name/enable_name',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
