<?php

namespace Kowal\LanguageFlags\Plugin;

/**
 * Class ShowFlagFrontend
 *
 * @package Kowal\LanguageFlags\Plugin
 */
class ShowFlagFrontend
{
    /**
     * @var \Kowal\LanguageFlags\Helper\Data
     */
    protected $helper;

    /**
     * ShowFlagFrontend constructor.
     *
     * @param \Kowal\LanguageFlags\Helper\Data $helper
     */
    public function __construct(
        \Kowal\LanguageFlags\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Plugin After
     *
     * @param \Magento\Store\Block\Switcher $subject
     * @param \Magento\Store\Block\Switcher $result
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetStores(\Magento\Store\Block\Switcher $subject, $result)
    {
        $data = [];
        if ($this->helper->getEndableModule()) {
            $storeIds = array_keys($result);
            foreach ($storeIds as $storeId) {
                if ($this->helper->getUrlImageFlag($storeId)) {
                    $data[$storeId] = [ 'width'  => $this->helper->getWidth($storeId),
                                        'height' => $this->helper->getHeight($storeId),
                                        'image'  => $this->helper->getUrlImageFlag($storeId),
                                        'show_label' => $this->helper->getShowStoreviewName($storeId)
                                        ];
                }
            }
        }
        $subject->setData('store_view_flag', $data);
        return $result;
    }
}
